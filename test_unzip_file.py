import os

from unittest import TestCase
from shutil import copyfile, rmtree

__test_tmp_path__ = 'tmp'
__fixture_path__ = 'fixture'
from unzip_file import unzip_file


class TestUnzip(TestCase):

    def setUp(self):
        try:
            rmtree(__test_tmp_path__)
        except Exception as e:
            pass
        os.mkdir(__test_tmp_path__)

    def tearDown(self):
        rmtree(__test_tmp_path__)


    def test_unzip_osx_zipped_move_to_root(self):
        filename = 'SGW.A6044.16.A1_2017-08-31_13-52-10.zip'
        input_file = os.path.join(__fixture_path__, filename)
        copyfile(input_file, os.path.join(__test_tmp_path__, filename))

        unzip_file(os.path.join(__test_tmp_path__, filename), move_to_root=True)

        files = os.listdir(__test_tmp_path__)
        self.assertEqual(7, len(files))
        self.assertTrue('170831135152798414109.jpg' in files)
        self.assertTrue('payload.json' in files)
        self.assertTrue('raw_201708311351.nav' in files)
        self.assertTrue('raw_201708311351.obs' in files)
        self.assertTrue('raw_201708311351.sbs' in files)
        self.assertTrue('raw_201708311351_RINEX-3_03.zip' in files)
        self.assertTrue(filename in files)


    def test_unzip_osx_zipped(self):
        filename = 'SGW.A6044.16.A1_2017-08-31_13-52-10.zip'
        input_file = os.path.join(__fixture_path__, filename)
        copyfile(input_file, os.path.join(__test_tmp_path__, filename))

        unzip_file(os.path.join(__test_tmp_path__, filename), move_to_root=False)

        files = os.listdir(__test_tmp_path__)
        self.assertEqual(5, len(files))
        self.assertTrue('raw_201708311351.nav' in files)
        self.assertTrue('raw_201708311351.obs' in files)
        self.assertTrue('raw_201708311351.sbs' in files)
        self.assertTrue(filename in files)
        
        files = os.listdir(os.path.join(__test_tmp_path__, 'SGW.A6044.16.A1_2017-08-31_13-52-10'))
        self.assertEqual(3, len(files))
        self.assertTrue('170831135152798414109.jpg' in files)
        self.assertTrue('payload.json' in files)
        self.assertTrue('raw_201708311351_RINEX-3_03.zip' in files)
