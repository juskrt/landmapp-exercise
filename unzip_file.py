import os
import traceback
import io
from logging import exception
from shutil import move, rmtree
from pathlib import Path
from zipfile import ZipFile


def extract(zip_file, f, extact_folder=None):
    try:
        zip_file.extract(f, extact_folder)
        print "Unzipped %s to %s" % (f, extact_folder)
    except:
        traceback.print_exc()
        print "Cannot unzip one item %s" % f

def unzip_file(file_path, move_to_root=True):
    path = Path(file_path)
    z = ZipFile(path.as_posix(), 'r')
    extract_folder = os.path.split(path.as_posix())[0]
    for f in z.namelist():
        if f.endswith('/'):
            continue
        if f.startswith('__'):
            continue
        if '/.' in f:
            continue
        if not f.endswith('zi p'):
            extract(z, f, extract_folder)
        else:
            extract(z, f, extract_folder)
            content = io.BytesIO(z.read(f))
            zip_file = ZipFile(content)
            for i in zip_file.namelist():
                if i.endswith('zip'):
                    ## avoid saving the file on disk and direclty unzip the content
                    sub_content = io.BytesIO(zip_file.read(i))
                    sub_zip_file = ZipFile(sub_content)
                    for j in sub_zip_file.namelist():
                        extract(sub_zip_file, j, extract_folder)
                else:
                    extract(zip_file, i, extract_folder)
        if '/' in f and move_to_root:
            if not os.path.isfile(os.path.join(extract_folder, f)):
                continue
            f_path = Path(f)
            sub = f_path.parent.as_posix()
            name = f_path.name
            move(os.path.join(extract_folder, f), os.path.join(extract_folder, name))
            rmtree(os.path.join(extract_folder, sub))
